import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
    state: () => {
        return {
            token: "",
            user: {}
        }
    },
    actions: {
        logout() {
            this.token = "";
            this.user = {};
        },
        login(token, user) {
            this.token = token;
            this.user = user;
        },
    }
})