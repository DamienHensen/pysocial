// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	devtools: { enabled: false },
	modules: ["nuxt-icons", "@nuxt/image", "@pinia/nuxt"],
	css: ["~/assets/css/main.css"],
	postcss: {
		plugins: {
			tailwindcss: {},
			autoprefixer: {},
		},
	},
	runtimeConfig: {
		public: {
			API_URL: "http://127.0.0.1:5000/api",
			CLOUDFRONT_URL: "https://d1irftsbvoenba.cloudfront.net/",
		},
	},
});
