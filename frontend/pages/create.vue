<script setup>
const config = useRuntimeConfig();
const authStore = useAuthStore();

const fileInput = ref(null);
const captionTextarea = ref(null);
const fileSrc = ref("/images/postplaceholder.jpg");
const postMedia = ref(null);

onMounted(() => {
	fixHeight();
});

const fixHeight = () => {
	captionTextarea.value.style.height = 0;
	captionTextarea.value.style.height = captionTextarea.value.scrollHeight + 5 + "px";
};

const changeFile = () => {
	const file = fileInput.value.files[0];
	const reader = new FileReader();

	reader.onload = function (e) {
		fileSrc.value = e.target.result;
	};

	if (file) {
		reader.readAsDataURL(file);
		postMedia.value = file;
	} else {
		fileSrc.value = "/images/postplaceholder.jpg";
		postMedia.value = null;
	}
};

const createPost = async () => {
	const form = new FormData();
	form.append("caption", captionTextarea.value.value);
	form.append("media", postMedia.value);

	await fetch(`${config.public.API_URL}/posts`, {
		method: "POST",
		body: form,
		headers: {
			Authorization: `Bearer ${authStore.token}`,
		},
	})
		.then((response) => response.json())
		.then(async (data) => {
			if (data.success) {
				await navigateTo("/");
			}
		})
		.catch(console.error);
};
</script>

<template>
	<Head>
		<title>Create a post</title>
	</Head>

	<div class="flex flex-col gap-4 pb-4">
		<label class="cursor-pointer">
			<NuxtImg ref="filePreview" :src="fileSrc" class="w-full object-contain min-h-96" />
			<input type="file" ref="fileInput" @change="changeFile" class="opacity-0 absolute w-0 h-0 block" />
		</label>

		<div class="px-2 flex flex-col gap-1">
			<label for="caption" class="text-lg">Caption</label>
			<textarea
				@input="fixHeight"
				ref="captionTextarea"
				id="caption"
				class="bg-transparent outline-none border-b border-slate-600 resize-none"
			></textarea>
		</div>

		<div class="ml-auto px-2">
			<button @click="createPost" class="bg-green-600 px-4 py-1 rounded text-lg uppercase font-medium transition-all hover:bg-green-800">
				Post!
			</button>
		</div>
	</div>
</template>
