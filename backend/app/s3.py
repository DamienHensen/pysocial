import os
import boto3
from dotenv import load_dotenv

load_dotenv()

aws_access_key = os.environ.get("AWS_ACCESS_KEY")
aws_secret_key = os.environ.get("AWS_SECRET_KEY")
aws_region = os.environ.get("AWS_REGION")
aws_bucket = os.environ.get("AWS_BUCKET")

s3 = boto3.client(
    "s3",
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
    region_name=aws_region,
)


def saveObject(object, filename):
    s3.put_object(Bucket=aws_bucket, Body=object, Key=filename)
    return filename
