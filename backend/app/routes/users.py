from app import app, db
from app.models import User
from flask import jsonify, request
from flask_jwt_extended import create_access_token
import bcrypt


@app.route("/api/users", methods=["GET"])
def listUsers():
    users = User.query.all()

    return jsonify([user.serialize() for user in users])


@app.route("/api/users/<int:user_id>", methods=["GET"])
def getUser(user_id):
    user = User.query.filter_by(id=user_id).first_or_404()

    return jsonify(user.serialize())


@app.route("/api/login", methods=["POST"])
def login():
    data = request.get_json()
    username = data.get("username")
    password = data.get("password")
    user = User.query.filter_by(username=username).first()

    if not username or not password or not user:
        return jsonify(success=False, msg="Username and/or Password is incorrect"), 401

    password = str.encode(password)

    correct_password = bcrypt.checkpw(password=password, hashed_password=user.password)

    if not correct_password:
        return jsonify(success=False, msg="Username and/or Password is incorrect"), 401

    additional_claims = {"role": user.role}
    token = create_access_token(user.id, additional_claims=additional_claims)

    return jsonify(success=True, token=token, user=user.serialize())


@app.route("/api/register", methods=["POST"])
def register():
    data = request.get_json()
    username = data.get("username")
    password = data.get("password")

    user = User.query.filter_by(username=data.get("username")).first()

    if user:
        return jsonify(success=False), 409

    bytes_password = str.encode(password)
    hashed_password = bcrypt.hashpw(bytes_password, bcrypt.gensalt())

    user = User(email=username, username=username, password=hashed_password)

    db.session.add(user)
    db.session.commit()

    return jsonify(success=True)
