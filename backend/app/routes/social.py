from flask import jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required
from app import app, db, s3
from app.models import Post, PostLike, User
from datetime import datetime


@app.route("/api/posts", methods=["GET"])
def listPosts():
    args = request.args

    if args.get("user"):
        posts = Post.query.filter_by(user_id=args.get("user")).all()
    else:
        posts = Post.query.all()

    post_list = []
    for post in posts:
        post_data = post.serialize()

        user = User.query.get(post.user_id)
        if user:
            post_data["user"] = user.serialize()

        post_list.append(post_data)

    return jsonify(post_list)


@app.route("/api/posts/<int:post_id>/like", methods=["POST"])
@jwt_required()
def likePost(post_id):
    post = Post.query.get(post_id)

    if post:
        user_id = get_jwt_identity()
        like = PostLike.query.filter_by(user_id=user_id, post_id=post_id).first()

        if like:
            db.session.delete(like)
        else:
            like = PostLike(user_id=user_id, post_id=post_id)
            db.session.add(like)

        db.session.commit()

        return jsonify(success=True)

    return jsonify(success=False)


@app.route("/api/posts", methods=["POST"])
@jwt_required()
def createPost():
    data = request.form
    unix_timestamp = datetime.now().timestamp()

    if "media" not in request.files:
        return jsonify(success=False)

    filename = "P" + str(unix_timestamp)
    mediaObject = request.files["media"]

    media = s3.saveObject(object=mediaObject, filename=filename)

    new_post = Post(
        user_id=get_jwt_identity(),
        caption=data.get("caption"),
        media=media,
    )

    db.session.add(new_post)
    db.session.commit()
    return jsonify(success=True, post=new_post.serialize())
