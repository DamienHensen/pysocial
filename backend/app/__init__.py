import os
from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from .models import db

load_dotenv()

app = Flask(__name__)

CORS(app)

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///../sqlite.db"
db.init_app(app)

app.config["JWT_SECRET_KEY"] = os.environ.get("JWT_SECRET")  # Change this!
jwt = JWTManager(app)

from app.routes import social, users
