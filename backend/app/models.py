import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

db = SQLAlchemy()


class UserRole(str, enum.Enum):
    user = "user"
    guest = "guest"
    admin = "admin"


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    avatar = db.Column(db.String(255))
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(72), nullable=False)
    posts = db.relationship("Post", backref="user")
    role = db.Column(db.Enum(UserRole), default=UserRole.user)

    def __init__(self, username, email, password, role=UserRole.user):
        self.username = username
        self.email = email
        self.password = password
        self.role = role

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "avatar": self.avatar,
        }


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    caption = db.Column(db.String(200))
    media = db.Column(db.String(255), nullable=False)
    likes = db.relationship("PostLike", backref="post")
    comments = db.relationship("Comment", backref="post")
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), onupdate=func.now())

    def __init__(self, user_id, caption, media):
        self.user_id = user_id
        self.caption = caption
        self.media = media

    def serialize(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "caption": self.caption,
            "media": self.media,
            "likes": [like.serialize() for like in self.likes],
            "comments": [comment.serialize() for comment in self.comments],
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey("post.id"), nullable=False)
    content = db.Column(db.String(200), nullable=False)
    likes = db.relationship("CommentLike", backref="comment")

    def __init__(self, user_id, post_id, content):
        self.user_id = user_id
        self.post_id = post_id
        self.content = content

    def serialize(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "post_id": self.post_id,
            "content": self.content,
            "likes": [like.serialize() for like in self.likes],
        }


class PostLike(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey("post.id"), nullable=False)

    def __init__(self, user_id, post_id):
        self.user_id = user_id
        self.post_id = post_id

    def serialize(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "post_id": self.post_id,
        }


class CommentLike(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    comment_id = db.Column(db.Integer, db.ForeignKey("comment.id"), nullable=False)

    def __init__(self, user_id, comment_id):
        self.user_id = user_id
        self.comment_id = comment_id

    def serialize(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "comment_id": self.comment_id,
        }
