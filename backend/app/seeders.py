from app import db
from app.models import Comment, CommentLike, Post, PostLike, User
import bcrypt


def seed_users():
    default_password = b"123123213"
    password1 = bcrypt.hashpw(default_password, bcrypt.gensalt())
    password2 = bcrypt.hashpw(default_password, bcrypt.gensalt())

    users = [
        User(username="joe", email="joe@damienx3.nl", password=password1),
        User(username="eric", email="eric@damienx3.nl", password=password2),
    ]

    db.session.bulk_save_objects(users)
    db.session.commit()


def seed_posts():
    posts = [
        Post(
            user_id=1,
            caption="Steve!",
            media="P1703724717.379978",
        ),
    ]

    comments = [Comment(user_id=2, post_id=1, content="Cool!")]

    likes = [
        PostLike(user_id=1, post_id=1),
        PostLike(user_id=2, post_id=1),
        CommentLike(user_id=1, comment_id=1),
    ]

    db.session.bulk_save_objects(posts)
    db.session.bulk_save_objects(comments)
    db.session.bulk_save_objects(likes)
    db.session.commit()
