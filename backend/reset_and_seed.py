from app import app, db
from app.seeders import seed_posts, seed_users

with app.app_context():
    db.drop_all()
    db.create_all()
    seed_users()
    seed_posts()
