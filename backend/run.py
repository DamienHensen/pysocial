from app import app, db
from app.seeders import seed_posts, seed_users

if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        seed_users()
        seed_posts()

    app.run(debug=True)
